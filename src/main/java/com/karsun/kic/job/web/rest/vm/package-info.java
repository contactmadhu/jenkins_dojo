/**
 * View Models used by Spring MVC REST controllers.
 */
package com.karsun.kic.job.web.rest.vm;
